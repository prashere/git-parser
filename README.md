I am learning how git organizes the files and folders internally, therefore this an attempt to write a simple git parser to make sure my understanding is correct.

1. Clone the repo.
2. Run the program

    python main.py /path/to/some/.git

If you don't have any other git repository in your computer, you can try this

    python main.py .git
