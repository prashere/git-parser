#!/bin/bash/env python

import sys
import zlib

def get_object_details(object_hash):
    folder, fname = object_hash[0:2], object_hash[2:]
    with open(OBJECTS_FOLDER + folder + "/" + fname, 'rb') as object_file:
        return zlib.decompress(object_file.read())

def get_latest_commit(branch):
    with open(BRANCH_FOLDER + branch, 'r') as branch:
        return branch.readline().strip()

def get_current_branch(head_file):
    with open(head_file, 'r') as head:
        return head.readline().strip().split(': ')[-1].split('/')[-1]

def main():
        # read HEAD
        current_branch = get_current_branch(GIT_FOLDER+'/HEAD')
        print ("HEAD -> {}".format(current_branch))

        print ("-" * 20)

        # read branch
        latest_commit = get_latest_commit(current_branch)
        print ("{} -> {}". format(current_branch, latest_commit))

        print ("-" * 20)

        # read object
        object_detail = get_object_details(latest_commit)
        print (object_detail.decode('utf-8'))

        print ("-" * 20)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print ("Program needs a path to .git directory. Try again")
        exit()
    else:
        GIT_FOLDER = sys.argv[1]
        BRANCH_FOLDER = GIT_FOLDER + '/refs/heads/'
        OBJECTS_FOLDER = GIT_FOLDER + '/objects/'
        main()
